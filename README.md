Ionic 2 Microsoft Face API - IT Talents Project 
=====================

## Information

This App is a project for the IT Talents Challange (Zeig uns Dein Gesicht)

[Project Link](https://www.it-talents.de/foerderung/code-competition/code-competition-03-2017)

Build with

 - Ionic 2
 - Ionic Native
 - Angular 2
 - Cordova

Supported Plattforms

 - Web Browser (Chrome, Firefox)
 - Android
 - iOS

## Demo

> Browser [Here](https://ittalents.nexyu.com/)
> Android [Here](https://ittalents.nexyu.com/android.apk)

## Getting Started
Clone this repository: 

    git clone git@gitlab.com:danielehrhardt/microsoft-face-detection.git
Run from the project root

    npm install

Install the ionic CLI

    npm install -g ionic

Run in a terminal from the project root

    ionic run browser


