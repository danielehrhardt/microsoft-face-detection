import { SecureStorage } from 'ionic-native';
import { ToastController, AlertController } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

@Injectable()
export class Functions {
  secureStorage: SecureStorage = new SecureStorage();

  constructor(public http: Http, public toastCtrl: ToastController, public alertCtrl: AlertController, public storage: Storage) {
    console.log('Hello Functions Provider');
  }

  showToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }


  clear() {
    return new Promise(resolve => {
      let storagemode: string;
      this.storage.get('storagemode').then((res) => {
        storagemode = res;
      })
      let confirm = this.alertCtrl.create({
        title: 'Bist du Sicher?',
        buttons: [
          {
            text: 'Abbrechen',
            handler: () => {
              console.log('Disagree clicked');
            }
          },
          {
            text: 'Löschen',
            handler: () => {
              if (storagemode == 'storage') {
                this.storage.set('security', '').then(() => {
                  this.storage.set('passwordlist', '').then(() => {
                    this.storage.set('storagemode', '').then(() => {
                      this.showToast('Deine Daten wurden gelöscht!');
                      resolve();
                    });
                  });
                });
              } else {
                this.storage.set('security', '').then(() => {
                  this.secureStorage.set('passwordlist', '').then(() => {
                    this.storage.set('storagemode', '').then(() => {
                      this.showToast('Deine Daten wurden gelöscht!');
                      resolve();
                    });
                  });
                });
              }
            }
          }
        ]
      });
      confirm.present();
    });
  }

}
