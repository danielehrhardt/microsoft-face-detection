import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class FaceRecognitionService {

    public subscriptionKey: string;
    public personGroupId: string;

    public data: any = null;

    constructor(public http: Http) {
        this.subscriptionKey = "66ad6c9931524bb899044b7d9ee9b43a";
    }

    /**
     * Verify a face from two face ids 
     * you can get a face id with the getFaceID Function
     * 
     * @param {any} faceid1 
     * @param {any} faceid2 
     * @returns {array} 
     * 
     * @memberOf FaceRecognitionService
     */
    verify(faceid1, faceid2) {
        console.log("Beginning AJAX request for Face Verify");

        return new Promise(resolve => {
            // We're using Angular Http provider to request the data,
            // then on the response it'll map the JSON data to a parsed JS object.
            // Next we process the data and resolve the promise with the new data.

            let body = JSON.stringify({ 'faceId1': faceid1, 'faceId2': faceid2 });
            let headers = new Headers({ 'Content-Type': 'application/json' });
            let options = new RequestOptions({ headers: headers });
            return this.http.post('https://westus.api.cognitive.microsoft.com/face/v1.0/verify?subscription-key=' + this.subscriptionKey, body, options)
                .map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                }, err => {
                    resolve(err);
                });
        });
    }


    /**
     * Upload image to imgur to get valide image url. This Image URL send to the Microsoft API
     * 
     * @param {any} base64 
     * @returns {string} 
     * 
     * @memberOf FaceRecognitionService
     */
    uploadImage(base64) {
        return new Promise(resolve => {
            // We're using Angular Http provider to request the data,
            // then on the response it'll map the JSON data to a parsed JS object.
            // Next we process the data and resolve the promise with the new data.

            let body = JSON.stringify({ 'image': base64 });
            let headers = new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Client-ID 6d3f7e22c0f5fbf'
            });
            let options = new RequestOptions({ headers: headers });
            return this.http.post('https://api.imgur.com/3/upload', body, options)
                .map(res => res.json())
                .subscribe(data => {
                    console.log('ImageURL: ' + data.data.link);
                    resolve(data.data.link);
                }, err => {
                    resolve(err);
                });
        });

    }

    /**
     * Get Faceid Response from Microsoft Face API with the image URL from imgur
     * 
     * @param {string} imageURL 
     * @returns 
     * 
     * @memberOf FaceRecognitionService
     */
    detectUrl(imageURL: string) {

        console.log("Beginning AJAX request for Face Detection");

        return new Promise(resolve => {
            // We're using Angular Http provider to request the data,
            // then on the response it'll map the JSON data to a parsed JS object.
            // Next we process the data and resolve the promise with the new data.

            let body = JSON.stringify({ 'url': imageURL });
            let headers = new Headers({ 'Content-Type': 'application/json' });
            let options = new RequestOptions({ headers: headers });
            return this.http.post('https://westus.api.cognitive.microsoft.com/face/v1.0/detect?subscription-key=' + this.subscriptionKey, body, options)
                .map(res => res.json())
                .subscribe(data => {
                    console.log(data);
                    if (data[0] != null) {
                        resolve(data[0]);
                    } else {
                        resolve(data);
                    }
                }, err => {
                    resolve(err);
                });
        });
    }

    /**
     * Convert the Response from the detect function to the faceid
     * 
     * @param {any} imageUrl 
     * @returns {string}
     * 
     * @memberOf FaceRecognitionService
     */
    getFaceId(imageUrl) {
        let response: any = [];
        return new Promise((resolve, reject) => {
            this.detectUrl(imageUrl).then((res) => {
                response = res;
                resolve(response.faceId);
            })
        });

    }

}