import { SecredDataPage } from '../pages/secred-data/secred-data';
import { RegisterPage } from './../pages/register/register';
import { LoginPage } from './../pages/login/login';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { Storage } from '@ionic/storage';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = RegisterPage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public storage: Storage) {
    this.initializeApp();


  }

  initializeApp() {
    this.storage.get('security').then((res) => {
          if (res) {
            this.rootPage = LoginPage;
          } else {
            this.rootPage = RegisterPage;
          }
        });

    this.platform.ready().then(() => {

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
    });

    this.platform.resume.subscribe(() => {
      if (this.nav.getActive().component === SecredDataPage){
        this.nav.setRoot(LoginPage, {}, {animate: true, direction: 'forward'});
      }
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component, {}, {animate: true, direction: 'forward'});
  }
}
