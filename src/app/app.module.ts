import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

// Ionic 2 Stuff
import { Storage } from '@ionic/storage';

// Services
import { FaceRecognitionService } from './../services/detection-service';
import { Functions } from './../providers/functions';

// Pages
import { SecredDataPage } from './../pages/secred-data/secred-data';
import { LoginPage } from './../pages/login/login';
import { RegisterPage } from './../pages/register/register';
import { SettingsPage } from './../pages/settings/settings';

//Components
import { UserHeaderComponent } from './../pages/user-header.component';

@NgModule({
  declarations: [
    MyApp,
    SecredDataPage,
    RegisterPage,
    LoginPage,
    SettingsPage,
    UserHeaderComponent
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SecredDataPage,
    RegisterPage,
    LoginPage,
    SettingsPage,
    UserHeaderComponent
  ],
  providers: [
    FaceRecognitionService,
    Storage,
    Functions,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
