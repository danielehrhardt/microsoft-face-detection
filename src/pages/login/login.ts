import { RegisterPage } from './../register/register';
import { SecredDataPage } from '../secred-data/secred-data';
import { FaceRecognitionService } from './../../services/detection-service';
import { Functions } from './../../providers/functions';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { Camera } from 'ionic-native';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  localfaceid: any;
  newfaceid: any;
  security_level: number;
  imageone: string = 'https://placeholdit.imgix.net/~text?txtsize=45&txt=?&w=100&h=100&txttrack=0';
  imagetwo: string;
  logindisabled: boolean = true;
  loading: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public functions: Functions,
    public storage: Storage,
    public faceService: FaceRecognitionService,
    public alertCtrl: AlertController) {


  }

  ionViewDidEnter() {
    this.storage.get('security').then((res) => {
      console.log(res);
      this.localfaceid = res.faceid;
      this.security_level = res.security_level;
      this.imageone = res.image;
    });
  }

  chooseImage() {
    let options = {
      quality: 70,
      targetWidth: 500,
      targetHeight: 500,
      destinationType: 0,
      correctOrientation: true
    };
    Camera.getPicture(options).then((base64) => {
      this.loading = true;
      this.faceService.uploadImage(base64).then((res) => {
        let response: any = res;
        this.imagetwo = response;
        console.log(this.imagetwo);
        this.loading = false;
      });

      this.logindisabled = false;
    }, (err) => {
      // Handle error
      this.functions.showToast('Fehler: ' + err);
      console.log(err);
    });
  }

  /**
   * Send the two Face IDs to the Microsoft API and check it with security level 
   * when correct redirect to the secured storage 
   * 
   * @memberOf LoginPage
   */
  login() {
    this.loading = true;
    this.faceService.getFaceId(this.imagetwo).then((res) => {
      this.newfaceid = res;
      this.functions.showToast('Bild wurde erkannt');

      console.log(this.localfaceid, this.newfaceid);
      this.faceService.verify(this.localfaceid, this.newfaceid).then((res) => {
        let response: any = res;

        console.log('Security:', this.security_level, (response.confidence * 100))
        if (this.security_level < (response.confidence * 100)) {
          let message = response.confidence * 100 + '% Übereinstimmung. OMG Du bist es. Schön dich wieder zu sehen 😍 Gleich gehts weiter 😎';
          setTimeout(() => {
            this.navCtrl.setRoot(SecredDataPage, {}, { animate: true, direction: 'forward' });
          }, 2000);
          this.functions.showToast(message)
          this.loading = false;
        } else {
          this.functions.showToast(response.confidence * 100 + '% Übereinstimmung. Ich glaube hier Versucht gerade die falsche Person sich Zugang zu unbefungten Daten zu verschaffen. Ich werden nun meinen Besitzer Informieren.');
          this.loading = false;
        }
      });
    }, (err) => {
      this.functions.showToast('Fehler: ' + err);
    });
  }


  clear() {
    this.functions.clear().then(() => {
      this.navCtrl.setRoot(RegisterPage, {}, { animate: true, direction: 'forward' });
    });;
  }

}
