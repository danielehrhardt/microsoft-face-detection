import { RegisterPage } from './../register/register';
import { SettingsPage } from './../settings/settings';
import { LoginPage } from './../login/login';
import { Functions } from './../../providers/functions';
import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Clipboard } from 'ionic-native';
import { SecureStorage } from 'ionic-native';

export interface PasswordEntry {
  name: string;
  user: string;
  password: string;
}


@Component({
  selector: 'page-secred-data',
  templateUrl: 'secred-data.html'
})
export class SecredDataPage {
  name: string;
  user: string;
  password: string;
  storagemode: string;

  passwordlist: any = [];

  SettingsPage = SettingsPage;
  secureStorage: SecureStorage = new SecureStorage();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public toastCtrl: ToastController,
    public functions: Functions,
    public alertCtrl: AlertController) {

    this.storage.get('storagemode').then((res) => {
      // Load Password List from Secure Storage
      if (res == 'storage') {
        //Fallback If Secure Storage is not working
        this.storagemode = res;
        this.storage.get('passwordlist').then((res) => {
          if (res) {
            this.passwordlist = JSON.parse(res);
          }
        });
      } else {
        this.secureStorage.create('secured_data').then(
          () => {
            console.log('Storage is ready!');
            this.storage.set('storagemode', 'securestorage');
            this.secureStorage.get('passwordlist').then(
              data => {
                this.storagemode = 'securestorage';
                this.passwordlist = JSON.parse(data);
              },
              error => console.log(error)
            );
          },
          error => {
            console.log("Fehler: " + error);
            console.log("Use Storage Fallback");
            this.storagemode = 'storage';
            this.storage.set('storagemode', 'storage');

          }
        );
      }
    });


  }


  showPassword(input: any): any {
    input.type = input.type === 'password' ? 'text' : 'password';
  }

  /**
   * Add Entry to List
   */
  addToList() {
    let data: PasswordEntry = {
      name: this.name,
      user: this.user,
      password: this.password

    };
    this.passwordlist.push(data);
    if (this.storagemode == 'storage') {
      this.storage.set('passwordlist', JSON.stringify(this.passwordlist)).then(() => {
        console.log('Entry added to List');
        this.functions.showToast('Eintrag hinzugefügt');
        this.name = '';
        this.user = '';
        this.password = '';
      });
    } else {
      this.secureStorage.set('passwordlist', JSON.stringify(this.passwordlist)).then(() => {
        console.log('Entry added to List');
        this.functions.showToast('Eintrag hinzugefügt');
        this.name = '';
        this.user = '';
        this.password = '';
      });
    }

  }

  copytoClipbord(password) {
    Clipboard.copy(password).then((res) => {
      this.functions.showToast('Erfolgreich in die Zwischenablage kopiert!');
    }, (err) => {
      this.functions.showToast('Fehler: ' + err);
    });

  }

  removeItem(item) {
    let confirm = this.alertCtrl.create({
      title: 'Eintrag löschen?',
      buttons: [
        {
          text: 'Abbrechen',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Löschen',
          handler: () => {
            for (let i = 0; i < this.passwordlist.length; i++) {
              if (this.passwordlist[i] == item) {
                this.passwordlist.splice(i, 1);
              }
            }
            if (this.storagemode == 'storage') {
              this.storage.set('passwordlist', JSON.stringify(this.passwordlist)).then(() => {
                this.functions.showToast('Deine Daten wurden gelöscht!');
              });
            } else {
              this.secureStorage.set('passwordlist', JSON.stringify(this.passwordlist)).then(() => {
                this.functions.showToast('Deine Daten wurden gelöscht!');
              });
            }
          }
        }
      ]
    });
    confirm.present();
  }


  logout() {
    this.navCtrl.setRoot(LoginPage, {}, { animate: true, direction: 'forward' });
  }

  clear() {
    this.functions.clear().then(() => {
      this.navCtrl.setRoot(RegisterPage, {}, { animate: true, direction: 'forward' });
    });;
  }




}
