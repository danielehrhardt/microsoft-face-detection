import { Functions } from '../providers/functions';
import { SecredDataPage } from './secred-data/secred-data';
import { FaceRecognitionService } from '../services/detection-service';
import { Component, Input } from '@angular/core';

import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { Camera } from 'ionic-native';
import { Storage } from '@ionic/storage';

export interface Security {
  faceid?: any;
  security_level?: number;
  image?: string;
}

@Component({
  selector: 'user-header',
  template: `
    <img [hidden]="!data.image" [src]="data.image">
    <img [hidden]="data.image" src="https://placeholdit.imgix.net/~text?txtsize=45&txt=?&w=100&h=100&txttrack=0">
    <button ion-button middle outline block (click)="chooseImage()" class="cta" color="primary" margin-bottom>Bild wählen</button>
    <p text-center>oder</p>
    <ion-item>
      <ion-input placeholder="https://url.com/img.jpg" [(ngModel)]="data.image" type="url"></ion-input>
    </ion-item> 
    

    <ion-item>
      <ion-range [(ngModel)]="data.security_level" min="0" max="100" color="primary" pin="true" snaps="true" step="10">
      </ion-range>
    </ion-item>

    <ion-label color="secondary" id="lbl">Sicherheitsstufe</ion-label>

     <button ion-button large outline block (click)="saveDatatoStorage()" class="cta" color="primary" [disabled]="loading || !data.image" icon-right>
         <span [hidden]="!(mode == 'register')">Tresor erstellen</span> 
         <span [hidden]="!(mode == 'settings')">Speichern</span> 
         <ion-spinner [hidden]="!loading" color="light"></ion-spinner>
    </button>
      <p color="light">{{ message }}</p>
      <p *ngIf="error">{{ error }}</p>   
  `
})

export class UserHeaderComponent {
  message: string = "";
  loading: boolean = false;
  data: Security = {
    security_level: 50
  };

  @Input() mode: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public faceService: FaceRecognitionService,
    public storage: Storage,
    public loadingCtrl: LoadingController,
    public functions: Functions) { }

  ngOnInit() {
    this.storage.get('security').then((res) => {
      if (res) {
        this.data = res;
      }
    });
  }

  ionViewDidEnter() {
    console.log(this.mode);
  }

  chooseImage() {
    let options = {
      quality: 70,
      targetWidth: 500,
      targetHeight: 500,
      destinationType: 0,
      correctOrientation: true
    };
    Camera.getPicture(options).then((base64) => {
      this.loading = true;
      this.faceService.uploadImage(base64).then((res) => {
        this.loading = false;
        let response: any = res;
        this.data.image = response;
        console.log(this.data.image);
      });
    }, (err) => {
      // Handle error
      console.log(err);
    });
  }

  saveDatatoStorage() {
    this.loading = true;


    this.changeMessage("Gesicht wird erkannt");

    this.faceService.getFaceId(this.data.image).then((res) => {


      if (typeof res == "undefined") {
        this.functions.showToast('Face ID Fehler');
        this.changeMessage("Fehler beim erkennen der Face ID");
        this.loading = false;
        return false;
      }

      this.data = {
        faceid: res,
        security_level: this.data.security_level,
        image: this.data.image
      };

      console.log(this.data);
      this.changeMessage("Gesicht wurde erkannt");

      this.storage.set('security', this.data).then(() => {

        this.changeMessage("Gesicht wurde gespeichert");

        //Loading Timeout für UX
        setTimeout(() => {
          this.loading = false;
        }, 3000);

        if (this.mode == 'register') {
          this.changeMessage("Sie werden nun zu Ihrem sicheren Ort weitergeleitet!");
          setTimeout(() => {
            //Redirect to Secure Data Page
            this.navCtrl.setRoot(SecredDataPage, {}, { animate: true, direction: 'forward' });
          }, 3000);
        } else if (this.mode == 'settings') {
          this.functions.showToast('Einstellungen gespeichert!');
          setTimeout(() => {
            this.navCtrl.pop();
          }, 3000);
        }
      });
    }, (err) => {
      this.functions.showToast('Fehler: ' + err);
      console.log('Fehler: ' + err);
    });
  }

  changeMessage(message) {
    setTimeout(() => {
      this.message = message;
    }, 500);
  }

}